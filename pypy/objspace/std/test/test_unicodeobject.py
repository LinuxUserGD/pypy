# -*- encoding: utf-8 -*-
import py
import sys
try:
    from hypothesis import given, strategies, settings, example
    HAS_HYPOTHESIS = True
except ImportError:
    HAS_HYPOTHESIS = False
    
from rpython.rlib import rutf8
from pypy.interpreter.error import OperationError


class TestUnicodeObject:
    def test_comparison_warning(self):
        warnings = []
        def my_warn(msg, warningscls):
            warnings.append(msg)
            prev_warn(msg, warningscls)
        space = self.space
        prev_warn = space.warn
        try:
            space.warn = my_warn
            space.appexec([], """():
                chr(128) == unichr(128)
                chr(128) != unichr(128)
                chr(127) == unichr(127) # no warnings
            """)
        finally:
            space.warn = prev_warn
        assert len(warnings) == 2

    def test_listview_ascii(self):
        w_str = self.space.newutf8('abcd', 4)
        assert self.space.listview_ascii(w_str) == list("abcd")

    def test_new_shortcut(self):
        space = self.space
        w_uni = self.space.newutf8('abcd', 4)
        w_new = space.call_method(
                space.w_unicode, "__new__", space.w_unicode, w_uni)
        assert w_new is w_uni

    def test_fast_iter(self):
        space = self.space
        w_uni = space.newutf8("aä".encode("utf-8"), 2)
        old_index_storage = w_uni._index_storage
        w_iter = space.iter(w_uni)
        w_char1 = w_iter.descr_next(space)
        w_char2 = w_iter.descr_next(space)
        py.test.raises(OperationError, w_iter.descr_next, space)
        assert w_uni._index_storage is old_index_storage
        assert space.eq_w(w_char1, w_uni._getitem_result(space, 0))
        assert space.eq_w(w_char2, w_uni._getitem_result(space, 1))


    if HAS_HYPOTHESIS:
        @given(strategies.text(), strategies.integers(min_value=0, max_value=10),
                                  strategies.integers(min_value=-1, max_value=10))
        def test_hypo_index_find(self, u, start, len1):
            if start + len1 < 0:
                return   # skip this case
            v = u[start : start + len1]
            space = self.space
            w_u = space.newutf8(u.encode('utf8'), len(u))
            w_v = space.newutf8(v.encode('utf8'), len(v))
            expected = u.find(v, start, start + len1)
            try:
                w_index = space.call_method(w_u, 'index', w_v,
                                            space.newint(start),
                                            space.newint(start + len1))
            except OperationError as e:
                if not e.match(space, space.w_ValueError):
                    raise
                assert expected == -1
            else:
                assert space.int_w(w_index) == expected >= 0

            w_index = space.call_method(w_u, 'find', w_v,
                                        space.newint(start),
                                        space.newint(start + len1))
            assert space.int_w(w_index) == expected

            rexpected = u.rfind(v, start, start + len1)
            try:
                w_index = space.call_method(w_u, 'rindex', w_v,
                                            space.newint(start),
                                            space.newint(start + len1))
            except OperationError as e:
                if not e.match(space, space.w_ValueError):
                    raise
                assert rexpected == -1
            else:
                assert space.int_w(w_index) == rexpected >= 0

            w_index = space.call_method(w_u, 'rfind', w_v,
                                        space.newint(start),
                                        space.newint(start + len1))
            assert space.int_w(w_index) == rexpected

            expected = u.startswith(v, start)
            w_res = space.call_method(w_u, 'startswith', w_v,
                                      space.newint(start))
            assert w_res is space.newbool(expected)

            expected = u.startswith(v, start, start + len1)
            w_res = space.call_method(w_u, 'startswith', w_v,
                                      space.newint(start),
                                      space.newint(start + len1))
            assert w_res is space.newbool(expected)

            expected = u.endswith(v, start)
            w_res = space.call_method(w_u, 'endswith', w_v,
                                      space.newint(start))
            assert w_res is space.newbool(expected)

            expected = u.endswith(v, start, start + len1)
            w_res = space.call_method(w_u, 'endswith', w_v,
                                      space.newint(start),
                                      space.newint(start + len1))
            assert w_res is space.newbool(expected)


        @given(u=strategies.text(),
               start=strategies.integers(min_value=0, max_value=10),
               len1=strategies.integers(min_value=-1, max_value=10))
        def test_hypo_index_find(self, u, start, len1):
            space = self.space
            if start + len1 < 0:
                return   # skip this case
            v = u[start : start + len1]
            w_u = space.wrap(u)
            w_v = space.wrap(v)
            expected = u.find(v, start, start + len1)
            try:
                w_index = space.call_method(w_u, 'index', w_v,
                                            space.newint(start),
                                            space.newint(start + len1))
            except OperationError as e:
                if not e.match(space, space.w_ValueError):
                    raise
                assert expected == -1
            else:
                assert space.int_w(w_index) == expected >= 0

            w_index = space.call_method(w_u, 'find', w_v,
                                        space.newint(start),
                                        space.newint(start + len1))
            assert space.int_w(w_index) == expected

            rexpected = u.rfind(v, start, start + len1)
            try:
                w_index = space.call_method(w_u, 'rindex', w_v,
                                            space.newint(start),
                                            space.newint(start + len1))
            except OperationError as e:
                if not e.match(space, space.w_ValueError):
                    raise
                assert rexpected == -1
            else:
                assert space.int_w(w_index) == rexpected >= 0

            w_index = space.call_method(w_u, 'rfind', w_v,
                                        space.newint(start),
                                        space.newint(start + len1))
            assert space.int_w(w_index) == rexpected

            expected = u.startswith(v, start)
            w_res = space.call_method(w_u, 'startswith', w_v,
                                      space.newint(start))
            assert w_res is space.newbool(expected)

            expected = u.startswith(v, start, start + len1)
            w_res = space.call_method(w_u, 'startswith', w_v,
                                      space.newint(start),
                                      space.newint(start + len1))
            assert w_res is space.newbool(expected)

            expected = u.endswith(v, start)
            w_res = space.call_method(w_u, 'endswith', w_v,
                                      space.newint(start))
            assert w_res is space.newbool(expected)

            expected = u.endswith(v, start, start + len1)
            w_res = space.call_method(w_u, 'endswith', w_v,
                                      space.newint(start),
                                      space.newint(start + len1))
            assert w_res is space.newbool(expected)

    def test_getitem_constant_index_jit(self):
        # test it directly, to prevent only seeing bugs in jitted code
        space = self.space
        u = "äöabc"
        w_u = self.space.wrap(u)
        for i in range(-len(u), len(u)):
            assert w_u._getitem_result_constant_index_jit(space, i)._utf8 == u[i].encode("utf-8")
        with py.test.raises(OperationError):
            w_u._getitem_result_constant_index_jit(space, len(u))
        with py.test.raises(OperationError):
            w_u._getitem_result_constant_index_jit(space, -len(u) - 1)

    def test_getslice_constant_index_jit(self):
        space = self.space
        u = "äöabcéééß"
        w_u = self.space.wrap(u)
        for start in range(0, 4):
            for end in range(start, len(u)):
                assert w_u._unicode_sliced_constant_index_jit(space, start, end)._utf8 == u[start: end].encode("utf-8")

    def test_lower_upper_ascii(self):
        from pypy.module.unicodedata.interp_ucd import unicodedb
        # check that ascii chars tolower/toupper still behave sensibly in the
        # unicodedb - unlikely to ever change, but well
        for ch in range(128):
            unilower, = unicodedb.tolower_full(ch)
            assert chr(unilower) == chr(ch).lower()
            uniupper, = unicodedb.toupper_full(ch)
            assert chr(uniupper) == chr(ch).upper()

    def test_latin1_ascii_encode_shortcut_ascii(self, monkeypatch):
        from rpython.rlib import rutf8
        from pypy.objspace.std.unicodeobject import encode_object
        monkeypatch.setattr(rutf8, "check_ascii", None)
        w_b = encode_object(self.space, self.space.newutf8("abc", 3), "latin-1", "strict")
        assert self.space.bytes_w(w_b) == "abc"
        w_b = encode_object(self.space, self.space.newutf8("abc", 3), "ascii", "strict")
        assert self.space.bytes_w(w_b) == "abc"

    def test_utf8_ascii_encode_shortcut_ascii(self, monkeypatch):
        from rpython.rlib import rutf8
        from pypy.objspace.std.unicodeobject import encode_object
        monkeypatch.setattr(rutf8, "has_surrogates", None)
        for enc in ["utf-8", "UTF-8", "utf8"]:
            w_b = encode_object(self.space, self.space.newutf8("abc", 3), enc, "strict")
            assert self.space.bytes_w(w_b) == "abc"

    def test_split_shortcut_ascii(self, monkeypatch):
        from rpython.rlib import rutf8
        monkeypatch.setattr(rutf8, "isspace", None)
        w_s = self.space.newutf8("a b c", 5)
        w_l = w_s.descr_split(self.space) # no crash
        assert self.space.len_w(w_l) == 3


class AppTestUnicodeStringStdOnly:
    def test_compares(self):
        assert 'a' == 'a'
        assert 'a' == 'a'
        assert not 'a' == 'b'
        assert not 'a'  == 'b'
        assert 'a' != 'b'
        assert 'a'  != 'b'
        assert not ('a' == 5)
        assert 'a' != 5
        assert 'a' < 5 or 'a' > 5

        s = chr(128)
        u = chr(128)
        assert not s == u # UnicodeWarning
        assert s != u
        assert not u == s
        assert u != s


class AppTestUnicodeString:
    spaceconfig = dict(usemodules=('unicodedata',))

    def test_addition(self):
        def check(a, b):
            assert a == b
            assert type(a) == type(b)
        check('a' + 'b', 'ab')
        check('a' + 'b', 'ab')

    def test_getitem(self):
        assert 'abc'[2] == 'c'
        raises(IndexError, 'abc'.__getitem__, 15)
        assert 'g\u0105\u015b\u0107'[2] == '\u015b'

    def test_join(self):
        def check(a, b):
            assert a == b
            assert type(a) == type(b)
        check(', '.join(['a']), 'a')
        check(', '.join(['a', 'b']), 'a, b')
        check(', '.join(['a', 'b']), 'a, b')
        try:
            ''.join(['a', 2, 3])
        except TypeError as e:
            assert 'sequence item 1' in str(e)
        else:
            raise Exception("DID NOT RAISE")

    if sys.version_info >= (2,3):
        def test_contains_ex(self):
            assert '' in 'abc'
            assert 'bc' in 'abc'
            assert 'bc' in 'abc'
        pass   # workaround for inspect.py bug in some Python 2.4s

    def test_contains(self):
        assert 'a' in 'abc'
        assert 'a' in 'abc'
        raises(UnicodeDecodeError, "u'\xe2' in 'g\xe2teau'")

    def test_splitlines(self):
        assert ''.splitlines() == []
        assert ''.splitlines(1) == []
        assert '\n'.splitlines() == ['']
        assert 'a'.splitlines() == ['a']
        assert 'one\ntwo'.splitlines() == ['one', 'two']
        assert '\ntwo\nthree'.splitlines() == ['', 'two', 'three']
        assert '\n\n'.splitlines() == ['', '']
        assert 'a\nb\nc'.splitlines(1) == ['a\n', 'b\n', 'c']
        assert '\na\nb\n'.splitlines(1) == ['\n', 'a\n', 'b\n']
        assert (('a' + '\xc2\x85'.decode('utf8') + 'b\n').splitlines() ==
                ['a', 'b'])

    def test_zfill(self):
        assert '123'.zfill(2) == '123'
        assert '123'.zfill(3) == '123'
        assert '123'.zfill(4) == '0123'
        assert '123'.zfill(6) == '000123'
        assert '+123'.zfill(2) == '+123'
        assert '+123'.zfill(3) == '+123'
        assert '+123'.zfill(4) == '+123'
        assert '+123'.zfill(5) == '+0123'
        assert '+123'.zfill(6) == '+00123'
        assert '-123'.zfill(3) == '-123'
        assert '-123'.zfill(4) == '-123'
        assert '-123'.zfill(5) == '-0123'
        assert ''.zfill(3) == '000'
        assert '34'.zfill(1) == '34'
        assert '34'.zfill(4) == '0034'

    def test_split(self):
        assert "".split() == []
        assert "".split('x') == ['']
        assert " ".split() == []
        assert "a".split() == ['a']
        assert "a".split("a", 1) == ['', '']
        assert " ".split(" ", 1) == ['', '']
        assert "aa".split("a", 2) == ['', '', '']
        assert " a ".split() == ['a']
        assert "a b c".split() == ['a','b','c']
        assert 'this is the split function'.split() == ['this', 'is', 'the', 'split', 'function']
        assert 'a|b|c|d'.split('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.split('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.split('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.split('|', 2) == ['a', 'b', 'c|d']
        assert 'a b c d'.split(None, 1) == ['a', 'b c d']
        assert 'a b c d'.split(None, 2) == ['a', 'b', 'c d']
        assert 'a b c d'.split(None, 3) == ['a', 'b', 'c', 'd']
        assert 'a b c d'.split(None, 4) == ['a', 'b', 'c', 'd']
        assert 'a b c d'.split(None, 0) == ['a b c d']
        assert 'a  b  c  d'.split(None, 2) == ['a', 'b', 'c  d']
        assert 'a b c d '.split() == ['a', 'b', 'c', 'd']
        assert 'a//b//c//d'.split('//') == ['a', 'b', 'c', 'd']
        assert 'endcase test'.split('test') == ['endcase ', '']
        raises(ValueError, 'abc'.split, '')
        raises(ValueError, 'abc'.split, '')
        raises(ValueError, 'abc'.split, '')
        assert '   a b c d'.split(None, 0) == ['a b c d']
        assert 'a\nb\u1680c'.split() == ['a', 'b', 'c']

    def test_rsplit(self):
        assert "".rsplit() == []
        assert " ".rsplit() == []
        assert "a".rsplit() == ['a']
        assert "a".rsplit("a", 1) == ['', '']
        assert " ".rsplit(" ", 1) == ['', '']
        assert "aa".rsplit("a", 2) == ['', '', '']
        assert " a ".rsplit() == ['a']
        assert "a b c".rsplit() == ['a','b','c']
        assert 'this is the rsplit function'.rsplit() == ['this', 'is', 'the', 'rsplit', 'function']
        assert 'a|b|c|d'.rsplit('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.rsplit('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.rsplit('|') == ['a', 'b', 'c', 'd']
        assert 'a|b|c|d'.rsplit('|', 2) == ['a|b', 'c', 'd']
        assert 'a b c d'.rsplit(None, 1) == ['a b c', 'd']
        assert 'a b c d'.rsplit(None, 2) == ['a b', 'c', 'd']
        assert 'a b c d'.rsplit(None, 3) == ['a', 'b', 'c', 'd']
        assert 'a b c d'.rsplit(None, 4) == ['a', 'b', 'c', 'd']
        assert 'a b c d'.rsplit(None, 0) == ['a b c d']
        assert 'a  b  c  d'.rsplit(None, 2) == ['a  b', 'c', 'd']
        assert 'a b c d '.rsplit() == ['a', 'b', 'c', 'd']
        assert 'a//b//c//d'.rsplit('//') == ['a', 'b', 'c', 'd']
        assert 'endcase test'.rsplit('test') == ['endcase ', '']
        raises(ValueError, 'abc'.rsplit, '')
        raises(ValueError, 'abc'.rsplit, '')
        raises(ValueError, 'abc'.rsplit, '')
        assert '  a b c  '.rsplit(None, 0) == ['  a b c']
        assert ''.rsplit('aaa') == ['']
        assert 'a\nb\u1680c'.rsplit() == ['a', 'b', 'c']

    def test_rsplit_bug(self):
        assert 'Vestur- og Mið'.rsplit() == ['Vestur-', 'og', 'Mið']

    def test_split_rsplit_str_unicode(self):
        x = 'abc'.split('b')
        assert x == ['a', 'c']
        assert list(map(type, x)) == [str, str]
        x = 'abc'.rsplit('b')
        assert x == ['a', 'c']
        assert list(map(type, x)) == [str, str]
        x = 'abc'.split('\u4321')
        assert x == ['abc']
        assert list(map(type, x)) == [str]
        x = 'abc'.rsplit('\u4321')
        assert x == ['abc']
        assert list(map(type, x)) == [str]
        raises(UnicodeDecodeError, '\x80'.split, 'a')
        raises(UnicodeDecodeError, '\x80'.split, '')
        raises(UnicodeDecodeError, '\x80'.rsplit, 'a')
        raises(UnicodeDecodeError, '\x80'.rsplit, '')

    def test_center(self):
        s="a b"
        assert s.center(0) == "a b"
        assert s.center(1) == "a b"
        assert s.center(2) == "a b"
        assert s.center(3) == "a b"
        assert s.center(4) == "a b "
        assert s.center(5) == " a b "
        assert s.center(6) == " a b  "
        assert s.center(7) == "  a b  "
        assert s.center(8) == "  a b   "
        assert s.center(9) == "   a b   "
        assert 'abc'.center(10) == '   abc    '
        assert 'abc'.center(6) == ' abc  '
        assert 'abc'.center(3) == 'abc'
        assert 'abc'.center(2) == 'abc'
        assert 'abc'.center(5, '*') == '*abc*'    # Python 2.4
        assert 'abc'.center(5, '*') == '*abc*'     # Python 2.4
        raises(TypeError, 'abc'.center, 4, 'cba')

    def test_title(self):
        assert "brown fox".title() == "Brown Fox"
        assert "!brown fox".title() == "!Brown Fox"
        assert "bROWN fOX".title() == "Brown Fox"
        assert "Brown Fox".title() == "Brown Fox"
        assert "bro!wn fox".title() == "Bro!Wn Fox"
        assert "brow\u4321n fox".title() == "Brow\u4321N Fox"
        assert '\ud800'.title() == '\ud800'
        assert (chr(0x345) + 'abc').title() == '\u0399Abc'
        assert (chr(0x345) + 'ABC').title() == '\u0399Abc'

    def test_istitle(self):
        assert "".istitle() == False
        assert "!".istitle() == False
        assert "!!".istitle() == False
        assert "brown fox".istitle() == False
        assert "!brown fox".istitle() == False
        assert "bROWN fOX".istitle() == False
        assert "Brown Fox".istitle() == True
        assert "bro!wn fox".istitle() == False
        assert "Bro!wn fox".istitle() == False
        assert "!brown Fox".istitle() == False
        assert "!Brown Fox".istitle() == True
        assert "Brow&&&&N Fox".istitle() == True
        assert "!Brow&&&&n Fox".istitle() == False
        assert '\u1FFc'.istitle()
        assert 'Greek \u1FFcitlecases ...'.istitle()

    def test_islower_isupper_with_titlecase(self):
        # \u01c5 is a char which is neither lowercase nor uppercase, but
        # titlecase
        assert not '\u01c5abc'.islower()
        assert not '\u01c5ABC'.isupper()

    def test_lower_upper(self):
        assert 'a'.lower() == 'a'
        assert 'A'.lower() == 'a'
        assert '\u0105'.lower() == '\u0105'
        assert '\u0104'.lower() == '\u0105'
        assert '\ud800'.lower() == '\ud800'
        assert 'a'.upper() == 'A'
        assert 'A'.upper() == 'A'
        assert '\u0105'.upper() == '\u0104'
        assert '\u0104'.upper() == '\u0104'
        assert '\ud800'.upper() == '\ud800'

    def test_capitalize(self):
        assert "brown fox".capitalize() == "Brown fox"
        assert ' hello '.capitalize() == ' hello '
        assert 'Hello '.capitalize() == 'Hello '
        assert 'hello '.capitalize() == 'Hello '
        assert 'aaaa'.capitalize() == 'Aaaa'
        assert 'AaAa'.capitalize() == 'Aaaa'
        # check that titlecased chars are lowered correctly
        # \u1ffc is the titlecased char
        assert ('\u1ff3\u1ff3\u1ffc\u1ffc'.capitalize() ==
                '\u1ffc\u1ff3\u1ff3\u1ff3')
        # check with cased non-letter chars
        assert ('\u24c5\u24ce\u24c9\u24bd\u24c4\u24c3'.capitalize() ==
                '\u24c5\u24e8\u24e3\u24d7\u24de\u24dd')
        assert ('\u24df\u24e8\u24e3\u24d7\u24de\u24dd'.capitalize() ==
                '\u24c5\u24e8\u24e3\u24d7\u24de\u24dd')
        assert '\u2160\u2161\u2162'.capitalize() == '\u2160\u2171\u2172'
        assert '\u2170\u2171\u2172'.capitalize() == '\u2160\u2171\u2172'
        # check with Ll chars with no upper - nothing changes here
        assert ('\u019b\u1d00\u1d86\u0221\u1fb7'.capitalize() ==
                '\u019b\u1d00\u1d86\u0221\u1fb7')
        assert '\ud800'.capitalize() == '\ud800'
        assert 'xx\ud800'.capitalize() == 'Xx\ud800'

    def test_rjust(self):
        s = "abc"
        assert s.rjust(2) == s
        assert s.rjust(3) == s
        assert s.rjust(4) == " " + s
        assert s.rjust(5) == "  " + s
        assert 'abc'.rjust(10) == '       abc'
        assert 'abc'.rjust(6) == '   abc'
        assert 'abc'.rjust(3) == 'abc'
        assert 'abc'.rjust(2) == 'abc'
        assert 'abc'.rjust(5, '*') == '**abc'    # Python 2.4
        assert 'abc'.rjust(5, '*') == '**abc'     # Python 2.4
        raises(TypeError, 'abc'.rjust, 5, 'xx')

    def test_ljust(self):
        s = "abc"
        assert s.ljust(2) == s
        assert s.ljust(3) == s
        assert s.ljust(4) == s + " "
        assert s.ljust(5) == s + "  "
        assert 'abc'.ljust(10) == 'abc       '
        assert 'abc'.ljust(6) == 'abc   '
        assert 'abc'.ljust(3) == 'abc'
        assert 'abc'.ljust(2) == 'abc'
        assert 'abc'.ljust(5, '*') == 'abc**'    # Python 2.4
        assert 'abc'.ljust(5, '*') == 'abc**'     # Python 2.4
        raises(TypeError, 'abc'.ljust, 6, '')

    def test_replace(self):
        assert 'one!two!three!'.replace('!', '@', 1) == 'one@two!three!'
        assert 'one!two!three!'.replace('!', '') == 'onetwothree'
        assert 'one!two!three!'.replace('!', '@', 2) == 'one@two@three!'
        assert 'one!two!three!'.replace('!', '@', 3) == 'one@two@three@'
        assert 'one!two!three!'.replace('!', '@', 4) == 'one@two@three@'
        assert 'one!two!three!'.replace('!', '@', 0) == 'one!two!three!'
        assert 'one!two!three!'.replace('!', '@') == 'one@two@three@'
        assert 'one!two!three!'.replace('x', '@') == 'one!two!three!'
        assert 'one!two!three!'.replace('x', '@', 2) == 'one!two!three!'
        assert 'abc'.replace('', '-') == '-a-b-c-'
        assert '\u1234'.replace('', '-') == '-\u1234-'
        assert '\u0234\u5678'.replace('', '-') == '-\u0234-\u5678-'
        assert '\u0234\u5678'.replace('', '-', 0) == '\u0234\u5678'
        assert '\u0234\u5678'.replace('', '-', 1) == '-\u0234\u5678'
        assert '\u0234\u5678'.replace('', '-', 2) == '-\u0234-\u5678'
        assert '\u0234\u5678'.replace('', '-', 3) == '-\u0234-\u5678-'
        assert '\u0234\u5678'.replace('', '-', 4) == '-\u0234-\u5678-'
        assert '\u0234\u5678'.replace('', '-', 700) == '-\u0234-\u5678-'
        assert '\u0234\u5678'.replace('', '-', -1) == '-\u0234-\u5678-'
        assert '\u0234\u5678'.replace('', '-', -42) == '-\u0234-\u5678-'
        assert 'abc'.replace('', '-', 3) == '-a-b-c'
        assert 'abc'.replace('', '-', 0) == 'abc'
        assert ''.replace('', '') == ''
        assert ''.replace('', 'a') == 'a'
        assert 'abc'.replace('ab', '--', 0) == 'abc'
        assert 'abc'.replace('xy', '--') == 'abc'
        assert '123'.replace('123', '') == ''
        assert '123123'.replace('123', '') == ''
        assert '123x123'.replace('123', '') == 'x'

    def test_replace_buffer(self):
        assert 'one!two!three'.replace(buffer('!'), buffer('@')) == 'one@two@three'

    def test_replace_overflow(self):
        import sys
        if sys.maxsize > 2**31-1:
            skip("Wrong platform")
        s = "a" * (2**16)
        raises(OverflowError, s.replace, "", s)

    def test_strip(self):
        s = " a b "
        assert s.strip() == "a b"
        assert s.rstrip() == " a b"
        assert s.lstrip() == "a b "
        assert 'xyzzyhelloxyzzy'.strip('xyz') == 'hello'
        assert 'xyzzyhelloxyzzy'.lstrip('xyz') == 'helloxyzzy'
        assert 'xyzzyhelloxyzzy'.rstrip('xyz') == 'xyzzyhello'
        exc = raises(TypeError, s.strip, buffer(' '))
        assert str(exc.value) == 'strip arg must be None, unicode or str'
        exc = raises(TypeError, s.rstrip, buffer(' '))
        assert str(exc.value) == 'rstrip arg must be None, unicode or str'
        exc = raises(TypeError, s.lstrip, buffer(' '))
        assert str(exc.value) == 'lstrip arg must be None, unicode or str'

    def test_strip_nonascii(self):
        s = u" ä b "
        assert s.strip() == u"ä b"
        assert s.rstrip() == u" ä b"
        assert s.lstrip() == u"ä b "
        assert u'xyzzyh³lloxyzzy'.strip(u'xyzü') == u'h³llo'
        assert u'xyzzyh³lloxyzzy'.lstrip(u'xyzü') == u'h³lloxyzzy'
        assert u'xyzzyh³lloxyzzy'.rstrip(u'xyzü') == u'xyzzyh³llo'

    def test_strip_str_unicode(self):
        x = "--abc--".strip("-")
        assert (x, type(x)) == ("abc", str)
        x = "--abc--".lstrip("-")
        assert (x, type(x)) == ("abc--", str)
        x = "--abc--".rstrip("-")
        assert (x, type(x)) == ("--abc", str)
        raises(UnicodeDecodeError, "\x80".strip, "")
        raises(UnicodeDecodeError, "\x80".lstrip, "")
        raises(UnicodeDecodeError, "\x80".rstrip, "")

    def test_rstrip_bug(self):
        assert u"aaaaaaaaaaaaaaaaaaa".rstrip(u"a") == u""
        assert u"äääääääääääääääääääääääää".rstrip(u"ä") == u""

    def test_long_from_unicode(self):
        assert int('12345678901234567890') == 12345678901234567890
        assert int('12345678901234567890') == 12345678901234567890
        assert int('123', 7) == 66

    def test_int_from_unicode(self):
        assert int('12345') == 12345

    def test_float_from_unicode(self):
        assert float('123.456e89') == float('123.456e89')

    def test_repr_16bits(self):
        # this used to fail when run on a CPython host with 16-bit unicodes
        s = repr('\U00101234')
        assert s == "u'\\U00101234'"

    def test_repr(self):
        for ustr in ["", "a", "'", "\'", "\"", "\t", "\\", '',
                     'a', '"', '\'', '\"', '\t', '\\', "'''\"",
                     chr(19), chr(2), '\u1234', '\U00101234']:
            assert eval(repr(ustr)) == ustr

    def test_getnewargs(self):
        class X(str):
            pass
        x = X("foo\u1234")
        a = x.__getnewargs__()
        assert a == ("foo\u1234",)
        assert type(a[0]) is str

    def test_call_unicode(self):
        assert str() == ''
        assert str(None) == 'None'
        assert str(123) == '123'
        assert str([2, 3]) == '[2, 3]'
        class U(str):
            pass
        assert str(U()).__class__ is str
        assert U('test') == 'test'
        assert U('test').__class__ is U

    def test_call_unicode_2(self):
        class X(object):
            def __unicode__(self):
                return 'x'
        raises(TypeError, str, X(), 'ascii')

    def test_startswith(self):
        assert 'ab'.startswith('ab') is True
        assert 'ab'.startswith('a') is True
        assert 'ab'.startswith('') is True
        assert 'x'.startswith('a') is False
        assert 'x'.startswith('x') is True
        assert ''.startswith('') is True
        assert ''.startswith('a') is False
        assert 'x'.startswith('xx') is False
        assert 'y'.startswith('xx') is False
        assert '\u1234\u5678\u4321'.startswith('\u1234') is True
        assert '\u1234\u5678\u4321'.startswith('\u1234\u4321') is False
        assert '\u1234'.startswith('', 1, 0) is True

    def test_startswith_more(self):
        assert 'ab'.startswith('a', 0) is True
        assert 'ab'.startswith('a', 1) is False
        assert 'ab'.startswith('b', 1) is True
        assert 'abc'.startswith('bc', 1, 2) is False
        assert 'abc'.startswith('c', -1, 4) is True

    def test_startswith_too_large(self):
        assert 'ab'.startswith('b', 1) is True
        assert 'ab'.startswith('', 2) is True
        assert 'ab'.startswith('', 3) is True   # not False
        assert 'ab'.endswith('b', 1) is True
        assert 'ab'.endswith('', 2) is True
        assert 'ab'.endswith('', 3) is True   # not False

    def test_startswith_tuples(self):
        assert 'hello'.startswith(('he', 'ha'))
        assert not 'hello'.startswith(('lo', 'llo'))
        assert 'hello'.startswith(('hellox', 'hello'))
        assert not 'hello'.startswith(())
        assert 'helloworld'.startswith(('hellowo', 'rld', 'lowo'), 3)
        assert not 'helloworld'.startswith(('hellowo', 'ello', 'rld'), 3)
        assert 'hello'.startswith(('lo', 'he'), 0, -1)
        assert not 'hello'.startswith(('he', 'hel'), 0, 1)
        assert 'hello'.startswith(('he', 'hel'), 0, 2)
        raises(TypeError, 'hello'.startswith, (42,))

    def test_startswith_endswith_convert(self):
        assert 'hello'.startswith(('he\u1111', 'he'))
        assert not 'hello'.startswith(('lo\u1111', 'llo'))
        assert 'hello'.startswith(('hellox\u1111', 'hello'))
        assert not 'hello'.startswith(('lo', 'he\u1111'), 0, -1)
        assert not 'hello'.endswith(('he\u1111', 'he'))
        assert 'hello'.endswith(('\u1111lo', 'llo'))
        assert 'hello'.endswith(('\u1111hellox', 'hello'))

    def test_endswith(self):
        assert 'ab'.endswith('ab') is True
        assert 'ab'.endswith('b') is True
        assert 'ab'.endswith('') is True
        assert 'x'.endswith('a') is False
        assert 'x'.endswith('x') is True
        assert ''.endswith('') is True
        assert ''.endswith('a') is False
        assert 'x'.endswith('xx') is False
        assert 'y'.endswith('xx') is False

    def test_endswith_more(self):
        assert 'abc'.endswith('ab', 0, 2) is True
        assert 'abc'.endswith('bc', 1) is True
        assert 'abc'.endswith('bc', 2) is False
        assert 'abc'.endswith('b', -3, -1) is True

    def test_endswith_tuple(self):
        assert not 'hello'.endswith(('he', 'ha'))
        assert 'hello'.endswith(('lo', 'llo'))
        assert 'hello'.endswith(('hellox', 'hello'))
        assert not 'hello'.endswith(())
        assert 'helloworld'.endswith(('hellowo', 'rld', 'lowo'), 3)
        assert not 'helloworld'.endswith(('hellowo', 'ello', 'rld'), 3, -1)
        assert 'hello'.endswith(('hell', 'ell'), 0, -1)
        assert not 'hello'.endswith(('he', 'hel'), 0, 1)
        assert 'hello'.endswith(('he', 'hell'), 0, 4)
        raises(TypeError, 'hello'.endswith, (42,))

    def test_expandtabs(self):
        assert 'abc\rab\tdef\ng\thi'.expandtabs() ==    'abc\rab      def\ng       hi'
        assert 'abc\rab\tdef\ng\thi'.expandtabs(8) ==   'abc\rab      def\ng       hi'
        assert 'abc\rab\tdef\ng\thi'.expandtabs(4) ==   'abc\rab  def\ng   hi'
        assert 'abc\r\nab\tdef\ng\thi'.expandtabs(4) == 'abc\r\nab  def\ng   hi'
        assert 'abc\rab\tdef\ng\thi'.expandtabs() ==    'abc\rab      def\ng       hi'
        assert 'abc\rab\tdef\ng\thi'.expandtabs(8) ==   'abc\rab      def\ng       hi'
        assert 'abc\r\nab\r\ndef\ng\r\nhi'.expandtabs(4) == 'abc\r\nab\r\ndef\ng\r\nhi'

        s = 'xy\t'
        assert s.expandtabs() =='xy      '

        s = '\txy\t'
        assert s.expandtabs() =='        xy      '
        assert s.expandtabs(1) ==' xy '
        assert s.expandtabs(2) =='  xy  '
        assert s.expandtabs(3) =='   xy '

        assert 'xy'.expandtabs() =='xy'
        assert ''.expandtabs() ==''

    def test_expandtabs_overflows_gracefully(self):
        import sys
        if sys.maxsize > (1 << 32):
            skip("Wrong platform")
        raises((OverflowError, MemoryError), 't\tt\t'.expandtabs, sys.maxsize)

    def test_expandtabs_0(self):
        assert 'x\ty'.expandtabs(0) == 'xy'
        assert 'x\ty'.expandtabs(-42) == 'xy'

    def test_translate(self):
        assert 'bbbc' == 'abababc'.translate({ord('a'):None})
        assert 'iiic' == 'abababc'.translate({ord('a'):None, ord('b'):ord('i')})
        assert 'iiix' == 'abababc'.translate({ord('a'):None, ord('b'):ord('i'), ord('c'):'x'})
        assert '<i><i><i>c' == 'abababc'.translate({ord('a'):None, ord('b'):'<i>'})
        assert 'c' == 'abababc'.translate({ord('a'):None, ord('b'):''})
        assert 'xyyx' == 'xzx'.translate({ord('z'):'yy'})
        assert 'abcd' == 'ab\0d'.translate('c')
        assert 'abcd' == 'abcd'.translate('')

        raises(TypeError, 'hello'.translate)
        raises(TypeError, 'abababc'.translate, {ord('a'):''})
        raises(TypeError, 'x'.translate, {ord('x'):0x110000})

    def test_unicode_from_encoded_object(self):
        assert str('x', 'utf-8') == 'x'
        assert str('x', 'utf-8', 'strict') == 'x'

    def test_unicode_startswith_tuple(self):
        assert 'xxx'.startswith(('x', 'y', 'z'), 0)
        assert 'xxx'.endswith(('x', 'y', 'z'), 0)

    def test_missing_cases(self):
        # some random cases, which are discovered to not be tested during annotation
        assert 'xxx'[1:1] == ''

    # these tests test lots of encodings, so they really belong to the _codecs
    # module. however, they test useful unicode methods too
    # they are stolen from CPython's unit tests

    def test_codecs_utf7(self):
        utfTests = [
            ('A\u2262\u0391.', 'A+ImIDkQ.'),             # RFC2152 example
            ('Hi Mom -\u263a-!', 'Hi Mom -+Jjo--!'),     # RFC2152 example
            ('\u65E5\u672C\u8A9E', '+ZeVnLIqe-'),        # RFC2152 example
            ('Item 3 is \u00a31.', 'Item 3 is +AKM-1.'), # RFC2152 example
            ('+', '+-'),
            ('+-', '+--'),
            ('+?', '+-?'),
            ('\?', '+AFw?'),
            ('+?', '+-?'),
            (r'\\?', '+AFwAXA?'),
            (r'\\\?', '+AFwAXABc?'),
            (r'++--', '+-+---'),
        ]

        for (x, y) in utfTests:
            assert x.encode('utf-7') == y

        # surrogates are supported
        assert str('+3ADYAA-', 'utf-7') == '\udc00\ud800'

        assert str('+AB', 'utf-7', 'replace') == '\ufffd'

    def test_codecs_utf8(self):
        assert ''.encode('utf-8') == ''
        assert '\u20ac'.encode('utf-8') == '\xe2\x82\xac'
        assert '\ud800\udc02'.encode('utf-8') == '\xf0\x90\x80\x82'
        assert '\ud84d\udc56'.encode('utf-8') == '\xf0\xa3\x91\x96'
        assert '\ud800\udc02'.encode('uTf-8') == '\xf0\x90\x80\x82'
        assert '\ud84d\udc56'.encode('Utf8') == '\xf0\xa3\x91\x96'
        assert '\ud800'.encode('utf-8') == '\xed\xa0\x80'
        assert '\udc00'.encode('utf-8') == '\xed\xb0\x80'
        assert ('\ud800\udc02'*1000).encode('utf-8') == '\xf0\x90\x80\x82'*1000
        assert (
            '\u6b63\u78ba\u306b\u8a00\u3046\u3068\u7ffb\u8a33\u306f'
            '\u3055\u308c\u3066\u3044\u307e\u305b\u3093\u3002\u4e00'
            '\u90e8\u306f\u30c9\u30a4\u30c4\u8a9e\u3067\u3059\u304c'
            '\u3001\u3042\u3068\u306f\u3067\u305f\u3089\u3081\u3067'
            '\u3059\u3002\u5b9f\u969b\u306b\u306f\u300cWenn ist das'
            ' Nunstuck git und'.encode('utf-8') == 
            '\xe6\xad\xa3\xe7\xa2\xba\xe3\x81\xab\xe8\xa8\x80\xe3\x81'
            '\x86\xe3\x81\xa8\xe7\xbf\xbb\xe8\xa8\xb3\xe3\x81\xaf\xe3'
            '\x81\x95\xe3\x82\x8c\xe3\x81\xa6\xe3\x81\x84\xe3\x81\xbe'
            '\xe3\x81\x9b\xe3\x82\x93\xe3\x80\x82\xe4\xb8\x80\xe9\x83'
            '\xa8\xe3\x81\xaf\xe3\x83\x89\xe3\x82\xa4\xe3\x83\x84\xe8'
            '\xaa\x9e\xe3\x81\xa7\xe3\x81\x99\xe3\x81\x8c\xe3\x80\x81'
            '\xe3\x81\x82\xe3\x81\xa8\xe3\x81\xaf\xe3\x81\xa7\xe3\x81'
            '\x9f\xe3\x82\x89\xe3\x82\x81\xe3\x81\xa7\xe3\x81\x99\xe3'
            '\x80\x82\xe5\xae\x9f\xe9\x9a\x9b\xe3\x81\xab\xe3\x81\xaf'
            '\xe3\x80\x8cWenn ist das Nunstuck git und'
        )

        # UTF-8 specific decoding tests
        assert str('\xf0\xa3\x91\x96', 'utf-8') == '\U00023456' 
        assert str('\xf0\x90\x80\x82', 'utf-8') == '\U00010002' 
        assert str('\xe2\x82\xac', 'utf-8') == '\u20ac' 

    def test_codecs_errors(self):
        # Error handling (encoding)
        raises(UnicodeError, 'Andr\202 x'.encode, 'ascii')
        raises(UnicodeError, 'Andr\202 x'.encode, 'ascii','strict')
        assert 'Andr\202 x'.encode('ascii','ignore') == "Andr x"
        assert 'Andr\202 x'.encode('ascii','replace') == "Andr? x"

        # Error handling (decoding)
        raises(UnicodeError, str, 'Andr\202 x', 'ascii')
        raises(UnicodeError, str, 'Andr\202 x', 'ascii','strict')
        assert str('Andr\202 x','ascii','ignore') == "Andr x"
        assert str('Andr\202 x','ascii','replace') == 'Andr\uFFFD x'

        # Error handling (unknown character names)
        assert "\\N{foo}xx".decode("unicode-escape", "ignore") == "xx"

        # Error handling (truncated escape sequence)
        raises(UnicodeError, "\\".decode, "unicode-escape")

        raises(UnicodeError, "\xc2".decode, "utf-8")
        assert '\xe1\x80'.decode('utf-8', 'replace') == "\ufffd"

    def test_repr_bug(self):
        assert (repr('\U00090418\u027d\U000582b9\u54c3\U000fcb6e') == 
                "u'\\U00090418\\u027d\\U000582b9\\u54c3\\U000fcb6e'")
        assert (repr('\n') == 
                "u'\\n'")


    def test_partition(self):
        assert ('this is the par', 'ti', 'tion method') == \
            'this is the partition method'.partition('ti')

        # from raymond's original specification
        S = 'http://www.python.org'
        assert ('http', '://', 'www.python.org') == S.partition('://')
        assert ('http://www.python.org', '', '') == S.partition('?')
        assert ('', 'http://', 'www.python.org') == S.partition('http://')
        assert ('http://www.python.', 'org', '') == S.partition('org')

        raises(ValueError, S.partition, '')
        raises(TypeError, S.partition, None)

    def test_rpartition(self):
        assert ('this is the rparti', 'ti', 'on method') == \
            'this is the rpartition method'.rpartition('ti')

        # from raymond's original specification
        S = 'http://www.python.org'
        assert ('http', '://', 'www.python.org') == S.rpartition('://')
        assert ('', '', 'http://www.python.org') == S.rpartition('?')
        assert ('', 'http://', 'www.python.org') == S.rpartition('http://')
        assert ('http://www.python.', 'org', '') == S.rpartition('org')

        raises(ValueError, S.rpartition, '')
        raises(TypeError, S.rpartition, None)

    def test_partition_str_unicode(self):
        x = 'abbbd'.rpartition('bb')
        assert x == ('ab', 'bb', 'd')
        assert list(map(type, x)) == [str, str, str]
        raises(UnicodeDecodeError, '\x80'.partition, '')
        raises(UnicodeDecodeError, '\x80'.rpartition, '')

    def test_mul(self):
        zero = 0
        assert type('' * zero) == type(zero * '') == str
        assert '' * zero == zero * '' == ''
        assert 'x' * zero == zero * 'x' == ''
        assert type('x' * zero) == type(zero * 'x') == str
        assert '123' * zero == zero * '123' == ''
        assert type('123' * zero) == type(zero * '123') == str
        for i in range(10):
            u = '123' * i
            assert len(u) == 3*i
            for j in range(0, i, 3):
                assert u[j+0] == '1'
                assert u[j+1] == '2'
                assert u[j+2] == '3'
            assert '123' * i == i * '123'

    def test_index(self):
        assert "rrarrrrrrrrra".index('a', 4, None) == 12
        assert "rrarrrrrrrrra".index('a', None, 6) == 2
        assert "\u1234\u4321\u5678".index('\u5678', 1) == 2

    def test_rindex(self):
        from sys import maxsize
        assert 'abcdefghiabc'.rindex('') == 12
        assert 'abcdefghiabc'.rindex('def') == 3
        assert 'abcdefghiabc'.rindex('abc') == 9
        assert 'abcdefghiabc'.rindex('abc', 0, -1) == 0
        assert 'abcdefghiabc'.rindex('abc', -4*maxint, 4*maxint) == 9
        assert 'rrarrrrrrrrra'.rindex('a', 4, None) == 12
        assert "\u1234\u5678".rindex('\u5678') == 1

        raises(ValueError, 'abcdefghiabc'.rindex, 'hib')
        raises(ValueError, 'defghiabc'.rindex, 'def', 1)
        raises(ValueError, 'defghiabc'.rindex, 'abc', 0, -1)
        raises(ValueError, 'abcdefghi'.rindex, 'ghi', 0, 8)
        raises(ValueError, 'abcdefghi'.rindex, 'ghi', 0, -1)
        raises(TypeError, 'abcdefghijklmn'.rindex, 'abc', 0, 0.0)
        raises(TypeError, 'abcdefghijklmn'.rindex, 'abc', -10.0, 30)

    def test_rfind(self):
        assert 'abcdefghiabc'.rfind('abc') == 9
        assert 'abcdefghiabc'.rfind('') == 12
        assert 'abcdefghiabc'.rfind('abcd') == 0
        assert 'abcdefghiabc'.rfind('abcz') == -1
        assert "\u1234\u5678".rfind('\u5678') == 1

    def test_rfind_corner_case(self):
        assert 'abc'.rfind('', 4) == -1

    def test_find_index_str_unicode(self):
        assert 'abcdefghiabc'.find('bc') == 1
        assert 'ab\u0105b\u0107'.find('b', 2) == 3
        assert 'ab\u0105b\u0107'.find('b', 0, 1) == -1
        assert 'abcdefghiabc'.rfind('abc') == 9
        raises(UnicodeDecodeError, '\x80'.find, '')
        raises(UnicodeDecodeError, '\x80'.rfind, '')
        assert 'abcdefghiabc'.index('bc') == 1
        assert 'abcdefghiabc'.rindex('abc') == 9
        raises(UnicodeDecodeError, '\x80'.index, '')
        raises(UnicodeDecodeError, '\x80'.rindex, '')
        assert "\u1234\u5678".find('\u5678') == 1

    def test_count_unicode(self):
        assert 'aaa'.count('', 10) == 0
        assert 'aaa'.count('', 3) == 1
        assert "".count("x") ==0
        assert "".count("") ==1
        assert "Python".count("") ==7
        assert "ab aaba".count("ab") ==2
        assert 'aaa'.count('a') == 3
        assert 'aaa'.count('b') == 0
        assert 'aaa'.count('a', -1) == 1
        assert 'aaa'.count('a', -10) == 3
        assert 'aaa'.count('a', 0, -1) == 2
        assert 'aaa'.count('a', 0, -10) == 0
        assert 'ababa'.count('aba') == 1

    def test_count_str_unicode(self):
        assert 'aaa'.count('a') == 3
        assert 'aaa'.count('b') == 0
        assert 'aaa'.count('a', -1) == 1
        assert 'aaa'.count('a', -10) == 3
        assert 'aaa'.count('a', 0, -1) == 2
        assert 'aaa'.count('a', 0, -10) == 0
        assert 'ababa'.count('aba') == 1
        raises(UnicodeDecodeError, '\x80'.count, '')

    def test_swapcase(self):
        assert '\xe4\xc4\xdf'.swapcase() == '\xc4\xe4\xdf'
        assert '\ud800'.swapcase() == '\ud800'

    def test_buffer(self):
        buf = buffer('XY')
        assert str(buf) in ['X\x00Y\x00',
                            '\x00X\x00Y',
                            'X\x00\x00\x00Y\x00\x00\x00',
                            '\x00\x00\x00X\x00\x00\x00Y']

    def test_call_special_methods(self):
        # xxx not completely clear if these are implementation details or not
        assert 'abc'.__add__('def') == 'abcdef'
        assert 'abc'.__add__('def') == 'abcdef'
        assert 'abc'.__add__('def') == 'abcdef'
        assert 'abc'.__rmod__('%s') == 'abc'
        ret = 'abc'.__rmod__('%s')
        raises(AttributeError, "u'abc'.__radd__(u'def')")

    def test_str_unicode_concat_overrides(self):
        "Test from Jython about being bug-compatible with CPython."

        def check(value, expected):
            assert type(value) == type(expected)
            assert value == expected

        def _test_concat(t1, t2):
            tprecedent = str
            if issubclass(t1, str) or issubclass(t2, str):
                tprecedent = str

            class SubclassB(t2):
                def __add__(self, other):
                    return SubclassB(t2(self) + t2(other))
            check(SubclassB('py') + SubclassB('thon'), SubclassB('python'))
            check(t1('python') + SubclassB('3'), tprecedent('python3'))
            check(SubclassB('py') + t1('py'), SubclassB('pypy'))

            class SubclassC(t2):
                def __radd__(self, other):
                    return SubclassC(t2(other) + t2(self))
            check(SubclassC('stack') + SubclassC('less'), t2('stackless'))
            check(t1('iron') + SubclassC('python'), SubclassC('ironpython'))
            check(SubclassC('tiny') + t1('py'), tprecedent('tinypy'))

            class SubclassD(t2):
                def __add__(self, other):
                    return SubclassD(t2(self) + t2(other))

                def __radd__(self, other):
                    return SubclassD(t2(other) + t2(self))
            check(SubclassD('di') + SubclassD('ct'), SubclassD('dict'))
            check(t1('list') + SubclassD(' comp'), SubclassD('list comp'))
            check(SubclassD('dun') + t1('der'), SubclassD('dunder'))

        _test_concat(str, str)
        _test_concat(str, str)
        # the following two cases are really there to emulate a CPython bug.
        _test_concat(str, str)   # uses hack in add__String_Unicode()
        _test_concat(str, str)   # uses hack in descroperation.binop_impl()

    def test_returns_subclass(self):
        class X(str):
            pass

        class Y(object):
            def __unicode__(self):
                return X("stuff")

        assert str(Y()).__class__ is X

    def test_getslice(self):
        assert '123456'.__getslice__(1, 5) == '2345'
        s = "\u0105b\u0107"
        assert s[:] == "\u0105b\u0107"
        assert s[1:] == "b\u0107"
        assert s[:2] == "\u0105b"
        assert s[1:2] == "b"
        assert s[-2:] == "b\u0107"
        assert s[:-1] == "\u0105b"
        assert s[-2:2] == "b"
        assert s[1:-1] == "b"
        assert s[-2:-1] == "b"

    def test_getitem_slice(self):
        assert '123456'.__getitem__(slice(1, 5)) == '2345'
        s = "\u0105b\u0107"
        assert s[slice(3)] == "\u0105b\u0107"
        assert s[slice(1, 3)] == "b\u0107"
        assert s[slice(2)] == "\u0105b"
        assert s[slice(1,2)] == "b"
        assert s[slice(-2,3)] == "b\u0107"
        assert s[slice(-1)] == "\u0105b"
        assert s[slice(-2,2)] == "b"
        assert s[slice(1,-1)] == "b"
        assert s[slice(-2,-1)] == "b"
        assert "abcde"[::2] == "ace"
        assert "\u0105\u0106\u0107abcd"[::2] == "\u0105\u0107bd"

    def test_no_len_on_str_iter(self):
        iterable = "hello"
        raises(TypeError, len, iter(iterable))

    def test_encode_raw_unicode_escape(self):
        u = str('\\', 'raw_unicode_escape')
        assert u == '\\'

    def test_decode_from_buffer(self):
        buf = buffer('character buffers are decoded to unicode')
        u = str(buf, 'utf-8', 'strict')
        assert u == 'character buffers are decoded to unicode'

    def test_unicode_conversion_with__unicode__(self):
        class A(str):
            def __unicode__(self):
                return "foo"
        class B(str):
            pass
        a = A('bar')
        assert a == 'bar'
        assert str(a) == 'foo'
        b = B('bar')
        assert b == 'bar'
        assert str(b) == 'bar'

    def test_unicode_conversion_with__str__(self):
        # new-style classes
        class A(object):
            def __str__(self):
                return '\u1234'
        s = str(A())
        assert type(s) is str
        assert s == '\u1234'
        # with old-style classes, it's different, but it should work as well
        class A:
            def __str__(self):
                return '\u1234'
        s = str(A())
        assert type(s) is str
        assert s == '\u1234'

    def test_formatting_unicode__str__(self):
        class A:
            def __init__(self, num):
                self.num = num
            def __str__(self):
                return chr(self.num)

        s = '%s' % A(111)    # this is ASCII
        assert type(s) is str
        assert s == chr(111)

        s = '%s' % A(0x1234)    # this is not ASCII
        assert type(s) is str
        assert s == '\u1234'

        # now the same with a new-style class...
        class A(object):
            def __init__(self, num):
                self.num = num
            def __str__(self):
                return chr(self.num)

        s = '%s' % A(111)    # this is ASCII
        assert type(s) is str
        assert s == chr(111)

        s = '%s' % A(0x1234)    # this is not ASCII
        assert type(s) is str
        assert s == '\u1234'

    def test_formatting_unicode__str__2(self):
        class A:
            def __str__(self):
                return 'baz'

        class B:
            def __str__(self):
                return 'foo'

            def __unicode__(self):
                return 'bar'

        a = A()
        b = B()
        s = '%s %s' % (a, b)
        assert s == 'baz bar'

        skip("but this case here is completely insane")
        s = '%s %s' % (b, a)
        assert s == 'foo baz'

    def test_formatting_unicode__str__3(self):
        # "bah" is all I can say
        class X(object):
            def __repr__(self):
                return '\u1234'
        '%s' % X()
        #
        class X(object):
            def __str__(self):
                return '\u1234'
        '%s' % X()

    def test_format_repeat(self):
        assert format("abc", "z<5") == "abczz"
        assert format("abc", "\u2007<5") == "abc\u2007\u2007"
        # raises UnicodeEncodeError, like CPython does
        raises(UnicodeEncodeError, format, 123, "\u2007<5")

    def test_formatting_char(self):
        for num in range(0x80,0x100):
            uchar = chr(num)
            print(num)
            assert uchar == "%c" % num   # works only with ints
            assert uchar == "%c" % uchar # and unicode chars
            # the implicit decoding should fail for non-ascii chars
            raises(UnicodeDecodeError, "%c".__mod__, chr(num))
            raises(UnicodeDecodeError, "%s".__mod__, chr(num))

    def test_str_subclass(self):
        class Foo9(str):
            def __unicode__(self):
                return "world"
        assert str(Foo9("hello")) == "world"

    def test_class_with_both_str_and_unicode(self):
        class A(object):
            def __str__(self):
                return 'foo'

            def __unicode__(self):
                return 'bar'

        assert str(A()) == 'bar'

        class A:
            def __str__(self):
                return 'foo'

            def __unicode__(self):
                return 'bar'

        assert str(A()) == 'bar'

    def test_format_unicode_subclass(self):
        class U(str):
            def __unicode__(self):
                return '__unicode__ overridden'
        u = U('xxx')
        assert repr("%s" % u) == "u'__unicode__ overridden'"
        assert repr("{}".format(u)) == "'__unicode__ overridden'"

    def test_format_c_overflow(self):
        import sys
        raises(OverflowError, '{0:c}'.format, -1)
        raises(OverflowError, '{0:c}'.format, sys.maxunicode + 1)

    def test_replace_with_buffer(self):
        assert 'abc'.replace(buffer('b'), buffer('e')) == 'aec'
        assert 'abc'.replace(buffer('b'), 'e') == 'aec'
        assert 'abc'.replace('b', buffer('e')) == 'aec'

    def test_unicode_subclass(self):
        class S(str):
            pass

        a = S('hello \u1234')
        b = str(a)
        assert type(b) is str
        assert b == 'hello \u1234'

        assert '%s' % S('mar\xe7') == 'mar\xe7'

    def test_isdecimal(self):
        assert '0'.isdecimal()
        assert not ''.isdecimal()
        assert not 'a'.isdecimal()
        assert not '\u2460'.isdecimal() # CIRCLED DIGIT ONE

    def test_isnumeric(self):
        assert '0'.isnumeric()
        assert not ''.isnumeric()
        assert not 'a'.isnumeric()
        assert '\u2460'.isnumeric() # CIRCLED DIGIT ONE

    def test_replace_str_unicode(self):
        res = 'one!two!three!'.replace('!', '@', 1)
        assert res == 'one@two!three!'
        assert type(res) == str
        raises(UnicodeDecodeError, '\x80'.replace, 'a', 'b')
        raises(UnicodeDecodeError, '\x80'.replace, 'a', 'b')

    def test_join_subclass(self):
        class UnicodeSubclass(str):
            pass
        class StrSubclass(str):
            pass

        s1 = UnicodeSubclass('a')
        assert ''.join([s1]) is not s1
        s2 = StrSubclass('a')
        assert ''.join([s2]) is not s2

    def test_encoding_and_errors_cant_be_none(self):
        raises(TypeError, "''.decode(None)")
        raises(TypeError, "u''.encode(None)")
        raises(TypeError, "unicode('', encoding=None)")
        raises(TypeError, 'u"".encode("utf-8", None)')

    def test_unicode_constructor_misc(self):
        x = 'foo'
        x += 'bar'
        assert str(x) is x
        #
        class U(str):
            def __unicode__(self):
                return 'BOK'
        u = U(x)
        assert str(u) == 'BOK'
        #
        class U2(str):
            pass
        z = U2('foobaz')
        assert type(str(z)) is str
        assert str(z) == 'foobaz'
        #
        assert str(encoding='supposedly_the_encoding') == ''
        assert str(errors='supposedly_the_error') == ''
        e = raises(TypeError, str, '', 'supposedly_the_encoding')
        assert str(e.value) == 'decoding Unicode is not supported'
        e = raises(TypeError, str, '', errors='supposedly_the_error')
        assert str(e.value) == 'decoding Unicode is not supported'
        e = raises(TypeError, str, u, 'supposedly_the_encoding')
        assert str(e.value) == 'decoding Unicode is not supported'
        e = raises(TypeError, str, z, 'supposedly_the_encoding')
        assert str(e.value) == 'decoding Unicode is not supported'

    def test_newlist_utf8_non_ascii(self):
        'ä'.split("\n")[0] # does not crash

    def test_replace_no_occurrence(self):
        x = "xyz"
        assert x.replace("a", "b") is x

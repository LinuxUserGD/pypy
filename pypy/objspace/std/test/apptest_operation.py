from pytest import raises

def teq(a, b):
    assert a == b
    assert type(a) is type(b)

def test_int_vs_long():
    # binary operators
    teq( 5  - 2  , 3  )
    teq( 5  - 2 , 3 )
    teq( 5 - 2  , 3 )
    teq( 5 - 2 , 3 )

    teq( 5  .__sub__(2 ), 3  )
    teq( 5  .__sub__(2), NotImplemented )
    teq( 5 .__sub__(2 ), 3 )
    teq( 5 .__sub__(2), 3 )

    teq( 5  .__rsub__(2 ), -3  )
    teq( 5  .__rsub__(2), NotImplemented )
    teq( 5 .__rsub__(2 ), -3 )
    teq( 5 .__rsub__(2), -3 )

    teq( 5  ** 2  , 25  )
    teq( 5  ** 2 , 25 )
    teq( 5 ** 2  , 25 )
    teq( 5 ** 2 , 25 )

    # ternary operator
    teq( pow( 5 , 3 , 100 ), 25 )
    teq( pow( 5 , 3 , 100), 25)
    teq( pow( 5 , 3, 100 ), 25)
    teq( pow( 5 , 3, 100), 25)
    teq( pow( 5, 3 , 100 ), 25)
    teq( pow( 5, 3 , 100), 25)
    teq( pow( 5, 3, 100 ), 25)
    teq( pow( 5, 3, 100), 25)

    # two tests give a different result on PyPy and CPython.
    # however, there is no sane way that PyPy can match CPython here,
    # short of reintroducing three-way coercion...
    teq( 5  .__pow__(3 , 100 ), 25 )
    #teq( 5  .__pow__(3 , 100L), 25L or NotImplemented? )
    teq( 5  .__pow__(3, 100 ), NotImplemented )
    teq( 5  .__pow__(3, 100), NotImplemented )
    teq( 5 .__pow__(3 , 100 ), 25)
    teq( 5 .__pow__(3 , 100), 25)
    teq( 5 .__pow__(3, 100 ), 25)
    teq( 5 .__pow__(3, 100), 25)

    teq( 5  .__rpow__(3 , 100 ), 43 )
    #teq( 5  .__rpow__(3 , 100L), 43L or NotImplemented? )
    teq( 5  .__rpow__(3, 100 ), NotImplemented )
    teq( 5  .__rpow__(3, 100), NotImplemented )
    teq( 5 .__rpow__(3 , 100 ), 43)
    teq( 5 .__rpow__(3 , 100), 43)
    teq( 5 .__rpow__(3, 100 ), 43)
    teq( 5 .__rpow__(3, 100), 43)


def test_int_vs_float():
    # binary operators
    teq( 5  - 2    , 3   )
    teq( 5  - 2.0  , 3.0 )
    teq( 5.0 - 2   , 3.0 )
    teq( 5.0 - 2.0 , 3.0 )

    teq( 5   .__sub__(2  ), 3   )
    teq( 5   .__sub__(2.0), NotImplemented )
    teq( 5.0 .__sub__(2  ), 3.0 )
    teq( 5.0 .__sub__(2.0), 3.0 )

    teq( 5   .__rsub__(2  ), -3   )
    teq( 5   .__rsub__(2.0), NotImplemented )
    teq( 5.0 .__rsub__(2  ), -3.0 )
    teq( 5.0 .__rsub__(2.0), -3.0 )

    teq( 5   ** 2   , 25   )
    teq( 5   ** 2.0 , 25.0 )
    teq( 5.0 ** 2   , 25.0 )
    teq( 5.0 ** 2.0 , 25.0 )

    # pow() fails with a float argument anywhere
    raises(TypeError, pow, 5  , 3  , 100.0)
    raises(TypeError, pow, 5  , 3.0, 100  )
    raises(TypeError, pow, 5  , 3.0, 100.0)
    raises(TypeError, pow, 5.0, 3  , 100  )
    raises(TypeError, pow, 5.0, 3  , 100.0)
    raises(TypeError, pow, 5.0, 3.0, 100  )
    raises(TypeError, pow, 5.0, 3.0, 100.0)

    teq( 5 .__pow__(3.0, 100  ), NotImplemented )
    teq( 5 .__pow__(3.0, 100.0), NotImplemented )

    teq( 5 .__rpow__(3.0, 100  ), NotImplemented )
    teq( 5 .__rpow__(3.0, 100.0), NotImplemented )


def test_long_vs_float():
    # binary operators
    teq( 5 - 2.0  , 3.0 )
    teq( 5.0 - 2  , 3.0 )

    teq( 5  .__sub__(2.0), NotImplemented )
    teq( 5.0 .__sub__(2 ), 3.0 )

    teq( 5  .__rsub__(2.0), NotImplemented )
    teq( 5.0 .__rsub__(2 ), -3.0 )

    teq( 5  ** 2.0 , 25.0 )
    teq( 5.0 ** 2  , 25.0 )

    # pow() fails with a float argument anywhere
    raises(TypeError, pow, 5 , 3 , 100.0)
    raises(TypeError, pow, 5 , 3.0, 100  )
    raises(TypeError, pow, 5 , 3.0, 100.0)
    raises(TypeError, pow, 5.0, 3 , 100  )
    raises(TypeError, pow, 5.0, 3 , 100.0)
    raises(TypeError, pow, 5.0, 3.0, 100  )
    raises(TypeError, pow, 5.0, 3.0, 100.0)

    teq( 5 .__pow__(3.0, 100 ), NotImplemented )
    teq( 5 .__pow__(3.0, 100.0), NotImplemented )

    teq( 5 .__rpow__(3.0, 100 ), NotImplemented )
    teq( 5 .__rpow__(3.0, 100.0), NotImplemented )

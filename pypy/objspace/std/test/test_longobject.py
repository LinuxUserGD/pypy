import py
from pypy.objspace.std import longobject as lobj
from rpython.rlib.rbigint import rbigint

class TestW_LongObject:
    def test_bigint_w(self):
        space = self.space
        fromlong = lobj.W_LongObject.fromlong
        assert isinstance(space.bigint_w(fromlong(42)), rbigint)
        assert space.bigint_w(fromlong(42)).eq(rbigint.fromint(42))
        assert space.bigint_w(fromlong(-1)).eq(rbigint.fromint(-1))
        w_obj = space.wrap("hello world")
        space.raises_w(space.w_TypeError, space.bigint_w, w_obj)
        w_obj = space.wrap(123.456)
        space.raises_w(space.w_TypeError, space.bigint_w, w_obj)

        w_obj = fromlong(42)
        assert space.unwrap(w_obj) == 42

    def test_overflow_error(self):
        space = self.space
        fromlong = lobj.W_LongObject.fromlong
        w_big = fromlong(10**900)
        space.raises_w(space.w_OverflowError, space.float_w, w_big)

    def test_rint_variants(self):
        from rpython.rtyper.tool.rfficache import platform
        space = self.space
        for r in list(platform.numbertype_to_rclass.values()):
            if r is int:
                continue
            print(r)
            values = [0, -1, r.MASK>>1, -(r.MASK>>1)-1]
            for x in values:
                if not r.SIGNED:
                    x &= r.MASK
                w_obj = space.newlong_from_rarith_int(r(x))
                assert space.bigint_w(w_obj).eq(rbigint.fromlong(x))


class AppTestLong:
    def test_trunc(self):
        import math
        assert math.trunc(1) == 1
        assert math.trunc(-1) == -1

    def test_add(self):
        x = 123
        assert int(x + 12443) == 123 + 12443
        x = -20
        assert x + 2 + 3 + True == -14

    def test_sub(self):
        assert int(58543 - 12332) == 58543 - 12332
        assert int(58543 - 12332) == 58543 - 12332
        assert int(58543 - 12332) == 58543 - 12332
        x = 237123838281233
        assert x * 12 == x * 12

    def test_mul(self):
        x = 363
        assert x * 2 ** 40 == x << 40

    def test_truediv(self):
        exec("from __future__ import division; a = 31415926L / 10000000L")
        assert a == 3.1415926

    def test_floordiv(self):
        x = 31415926
        a = x // 10000000
        assert a == 3

    def test_int_floordiv(self):
        import sys

        x = 3000
        a = x // 1000
        assert a == 3

        x = 3000
        a = x // -1000
        assert a == -3

        x = 3000
        raises(ZeroDivisionError, "x // 0")

        n = sys.maxsize+1
        assert n / int(-n) == -1

    def test_numerator_denominator(self):
        assert (1).numerator == 1
        assert (1).denominator == 1
        assert (42).numerator == 42
        assert (42).denominator == 1

    def test_compare(self):
        Z = 0
        ZL = 0

        assert Z == ZL
        assert not (Z != ZL)
        assert ZL == Z
        assert not (ZL != Z)
        assert Z <= ZL
        assert not (Z < ZL)
        assert ZL <= ZL
        assert not (ZL < ZL)

        for BIG in (1, 1 << 62, 1 << 9999):
            assert not (Z == BIG)
            assert Z != BIG
            assert not (BIG == Z)
            assert BIG != Z
            assert not (ZL == BIG)
            assert ZL != BIG
            assert Z <= BIG
            assert Z < BIG
            assert not (BIG <= Z)
            assert not (BIG < Z)
            assert ZL <= BIG
            assert ZL < BIG
            assert not (BIG <= ZL)
            assert not (BIG < ZL)
            assert not (Z <= -BIG)
            assert not (Z < -BIG)
            assert -BIG <= Z
            assert -BIG < Z
            assert not (ZL <= -BIG)
            assert not (ZL < -BIG)
            assert -BIG <= ZL
            assert -BIG < ZL
            #
            assert not (BIG <  int(BIG))
            assert     (BIG <= int(BIG))
            assert     (BIG == int(BIG))
            assert not (BIG != int(BIG))
            assert not (BIG >  int(BIG))
            assert     (BIG >= int(BIG))
            #
            assert     (BIG <  int(BIG)+1)
            assert     (BIG <= int(BIG)+1)
            assert not (BIG == int(BIG)+1)
            assert     (BIG != int(BIG)+1)
            assert not (BIG >  int(BIG)+1)
            assert not (BIG >= int(BIG)+1)
            #
            assert not (BIG <  int(BIG)-1)
            assert not (BIG <= int(BIG)-1)
            assert not (BIG == int(BIG)-1)
            assert     (BIG != int(BIG)-1)
            assert     (BIG >  int(BIG)-1)
            assert     (BIG >= int(BIG)-1)
            #
            assert not (int(BIG) <  BIG)
            assert     (int(BIG) <= BIG)
            assert     (int(BIG) == BIG)
            assert not (int(BIG) != BIG)
            assert not (int(BIG) >  BIG)
            assert     (int(BIG) >= BIG)
            #
            assert not (int(BIG)+1 <  BIG)
            assert not (int(BIG)+1 <= BIG)
            assert not (int(BIG)+1 == BIG)
            assert     (int(BIG)+1 != BIG)
            assert     (int(BIG)+1 >  BIG)
            assert     (int(BIG)+1 >= BIG)
            #
            assert     (int(BIG)-1 <  BIG)
            assert     (int(BIG)-1 <= BIG)
            assert not (int(BIG)-1 == BIG)
            assert     (int(BIG)-1 != BIG)
            assert not (int(BIG)-1 >  BIG)
            assert not (int(BIG)-1 >= BIG)

    def test_conversion(self):
        class long2(long):
            pass
        x = 1
        x = long2(x<<100)
        y = int(x)
        assert type(y) == int
        assert type(+long2(5)) is int
        assert type(long2(5) << 0) is int
        assert type(long2(5) >> 0) is int
        assert type(long2(5) + 0) is int
        assert type(long2(5) - 0) is int
        assert type(long2(5) * 1) is int
        assert type(1 * long2(5)) is int
        assert type(0 + long2(5)) is int
        assert type(-long2(0)) is int
        assert type(long2(5) // 1) is int

    def test_shift(self):
        assert 65 >> 2 == 16
        assert 65 >> 2 == 16
        assert 65 >> 2 == 16
        assert 65 << 2 == 65 * 4
        assert 65 << 2 == 65 * 4
        assert 65 << 2 == 65 * 4
        raises(ValueError, "1L << -1L")
        raises(ValueError, "1L << -1")
        raises(OverflowError, "1L << (2 ** 100)")
        raises(ValueError, "1L >> -1L")
        raises(ValueError, "1L >> -1")
        raises(OverflowError, "1L >> (2 ** 100)")

    def test_pow(self):
        x = 0
        assert pow(x, 0, 1) == 0
        assert pow(-1, -1) == -1.0
        assert pow(2 ** 68, 0.5) == 2.0 ** 34
        assert pow(2 ** 68, 2) == 2 ** 136
        raises(TypeError, pow, 2, -1, 3)
        raises(ValueError, pow, 2, 5, 0)

        # some rpow tests
        assert pow(0, 0, 1) == 0
        assert pow(-1, -1) == -1.0

    def test_int_pow(self):
        x = 2
        assert pow(x, 2) == 4
        assert pow(x, 2, 2) == 0
        assert pow(x, 2, 3) == 1

    def test_getnewargs(self):
        assert  0 .__getnewargs__() == (0,)
        assert  (-1) .__getnewargs__() == (-1,)

    def test_divmod(self):
        def check_division(x, y):
            q, r = divmod(x, y)
            pab, pba = x*y, y*x
            assert pab == pba
            assert q == x // y
            assert r == x % y
            assert x == q*y + r
            if y > 0:
                assert 0 <= r < y
            else:
                assert y < r <= 0
        for x in [-1, 0, 1, 2 ** 100 - 1, -2 ** 100 - 1]:
            for y in [-105566530, -1, 1, 1034522340]:
                print("checking division for %s, %s" % (x, y))
                check_division(x, y)
                check_division(x, int(y))
                check_division(int(x), y)
        # special case from python tests:
        s1 = 33
        s2 = 2
        x = 16565645174462751485571442763871865344588923363439663038777355323778298703228675004033774331442052275771343018700586987657790981527457655176938756028872904152013524821759375058141439
        x >>= s1*16
        y = 10953035502453784575
        y >>= s2*16
        x = 0x3FE0003FFFFC0001FFF
        y = 0x9800FFC1
        check_division(x, y)
        raises(ZeroDivisionError, "x // 0L")
        raises(ZeroDivisionError, "x % 0L")
        raises(ZeroDivisionError, divmod, x, 0)
        raises(ZeroDivisionError, "x // 0")
        raises(ZeroDivisionError, "x % 0")
        raises(ZeroDivisionError, divmod, x, 0)

    def test_int_divmod(self):
        q, r = divmod(100, 11)
        assert q == 9
        assert r == 1

    def test_format(self):
        assert repr(12345678901234567890) == '12345678901234567890L'
        assert str(12345678901234567890) == '12345678901234567890'
        assert hex(0x1234567890ABCDEF) == '0x1234567890abcdefL'
        assert oct(01234567012345670) == '01234567012345670L'

    def test_bits(self):
        x = 0xAAAAAAAA
        assert x | 0x55555555 == 0xFFFFFFFF
        assert x & 0x55555555 == 0x00000000
        assert x ^ 0x55555555 == 0xFFFFFFFF
        assert -x | 0x55555555 == -0xAAAAAAA9
        assert x | 0x555555555 == 0x5FFFFFFFF
        assert x & 0x555555555 == 0x000000000
        assert x ^ 0x555555555 == 0x5FFFFFFFF

    def test_hash(self):
        # ints have the same hash as equal longs
        for i in range(-4, 14):
            assert hash(i) == hash(int(i)) == int(i).__hash__()
        # might check too much -- it's ok to change the hashing algorithm
        assert hash(123456789) == 123456789
        assert hash(1234567890123456789) in (
            -1895067127,            # with 32-bit platforms
            1234567890123456789)    # with 64-bit platforms

    def test_math_log(self):
        import math
        raises(ValueError, math.log, 0)
        raises(ValueError, math.log, -1)
        raises(ValueError, math.log, -2)
        raises(ValueError, math.log, -(1 << 10000))
        #raises(ValueError, math.log, 0)
        raises(ValueError, math.log, -1)
        raises(ValueError, math.log, -2)

    def test_long(self):
        import sys
        n = -sys.maxsize-1
        assert int(n) == n
        assert str(int(n)) == str(n)
        a = buffer('123')
        assert int(a) == 123

    def test_huge_longs(self):
        import operator
        x = 1
        huge = x << 40000
        raises(OverflowError, float, huge)
        raises(OverflowError, operator.truediv, huge, 3)
        raises(OverflowError, operator.truediv, huge, 3)

    def test_just_trunc(self):
        class myint(object):
            def __trunc__(self):
                return 42
        assert int(myint()) == 42

    def test_override___long__(self):
        class mylong(long):
            def __long__(self):
                return 42
        assert int(mylong(21)) == 42
        class myotherlong(long):
            pass
        assert int(myotherlong(21)) == 21

    def test___long__(self):
        class A(object):
            def __long__(self):
                return 42
        assert int(A()) == 42
        class B(object):
            def __int__(self):
                return 42
        raises(TypeError, int, B())

        class LongSubclass(long):
            pass
        class ReturnsLongSubclass(object):
            def __long__(self):
                return LongSubclass(42)
        n = int(ReturnsLongSubclass())
        assert n == 42
        assert type(n) is LongSubclass

    def test_trunc_returns(self):
        # but!: (blame CPython 2.7)
        class Integral(object):
            def __int__(self):
                return 42
        class TruncReturnsNonLong(object):
            def __trunc__(self):
                return Integral()
        n = int(TruncReturnsNonLong())
        assert type(n) is int
        assert n == 42

        class LongSubclass(long):
            pass
        class TruncReturnsNonInt(object):
            def __trunc__(self):
                return LongSubclass(42)
        n = int(TruncReturnsNonInt())
        assert n == 42
        assert type(n) is LongSubclass

    def test_long_before_string(self):
        class A(str):
            def __long__(self):
                return 42
        assert int(A('abc')) == 42

    def test_long_errors(self):
        raises(TypeError, int, 12, 12)
        raises(ValueError, int, 'xxxxxx?', 12)

    def test_conjugate(self):
        assert (7).conjugate() == 7
        assert (-7).conjugate() == -7

        class L(long):
            pass

        assert type(L(7).conjugate()) is int

        class L(long):
            def __pos__(self):
                return 43
        assert L(7).conjugate() == 7

    def test_bit_length(self):
        assert 8.bit_length() == 4
        assert (-1<<40).bit_length() == 41
        assert ((2**31)-1).bit_length() == 31

    def test_negative_zero(self):
        x = eval("-0L")
        assert x == 0

    def test_mix_int_and_long(self):
        class IntLongMixClass(object):
            def __int__(self):
                return 42

            def __long__(self):
                return 64

        mixIntAndLong = IntLongMixClass()
        as_long = int(mixIntAndLong)
        assert type(as_long) is int
        assert as_long == 64

    def test_long_real(self):
        class A(long): pass
        b = A(5).real
        assert type(b) is int

    def test__int__(self):
        class A(long):
            def __int__(self):
                return 42

        assert int(int(3)) == int(3)
        assert int(A(13)) == 42

    def test_long_error_msg(self):
        e = raises(TypeError, int, [])
        assert str(e.value) == (
            "long() argument must be a string or a number, not 'list'")

    def test_coerce(self):
        assert 3.__coerce__(4) == (3, 4)
        assert 3.__coerce__(4) == (3, 4)
        assert 3.__coerce__(object()) == NotImplemented

    def test_linear_long_base_16(self):
        # never finishes if long(_, 16) is not linear-time
        size = 100000
        n = "a" * size
        expected = (2 << (size * 4)) // 3
        assert int(n, 16) == expected

    def test_error_message_wrong_self(self):
        unboundmeth = int.__str__
        e = raises(TypeError, unboundmeth, 42)
        assert "long" in str(e.value)
        if hasattr(unboundmeth, 'im_func'):
            e = raises(TypeError, unboundmeth.__func__, 42)
            assert "'long'" in str(e.value)


class AppTest(object):
    spaceconfig = {"objspace.usemodules._pypyjson": True}

    def test_check_strategy(self):
        import __pypy__
        import _pypyjson

        d = _pypyjson.loads('{"a": 1}')
        assert __pypy__.strategy(d) == "JsonDictStrategy"
        d = _pypyjson.loads('{}')
        assert __pypy__.strategy(d) == "EmptyDictStrategy"

    def test_simple(self):
        import __pypy__
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        assert len(d) == 2
        assert d["a"] == 1
        assert d["b"] == "x"
        assert "c" not in d

        d["a"] = 5
        assert d["a"] == 5
        assert __pypy__.strategy(d) == "JsonDictStrategy"

        # devolve it
        assert not 1 in d
        assert __pypy__.strategy(d) == "UnicodeDictStrategy"
        assert len(d) == 2
        assert d["a"] == 5
        assert d["b"] == "x"
        assert "c" not in d

    def test_setdefault(self):
        import __pypy__
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        assert d.setdefault("a", "blub") == 1
        d.setdefault("x", 23)
        assert __pypy__.strategy(d) == "UnicodeDictStrategy"
        assert len(d) == 3
        assert d == {"a": 1, "b": "x", "x": 23}

    def test_delitem(self):
        import __pypy__
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        del d["a"]
        assert __pypy__.strategy(d) == "UnicodeDictStrategy"
        assert len(d) == 1
        assert d == {"b": "x"}

    def test_popitem(self):
        import __pypy__
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        k, v = d.popitem()
        assert __pypy__.strategy(d) == "UnicodeDictStrategy"
        if k == "a":
            assert v == 1
            assert len(d) == 1
            assert d == {"b": "x"}
        else:
            assert v == "x"
            assert len(d) == 1
            assert d == {"a": 1}

    def test_keys_value_items(self):
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        assert list(d.keys()) == ["a", "b"]
        assert list(d.values()) == [1, "x"]
        assert list(d.items()) == [("a", 1), ("b", "x")]

    def test_iter_keys_value_items(self):
        import _pypyjson

        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        assert list(d.keys()) == ["a", "b"]
        assert list(d.values()) == [1, "x"]
        assert list(d.items()) == [("a", 1), ("b", "x")]

    def test_dict_order_retained_when_switching_strategies(self):
        import _pypyjson
        import __pypy__
        d = _pypyjson.loads('{"a": 1, "b": "x"}')
        assert list(d) == ["a", "b"]
        # devolve
        assert not 1 in d
        assert __pypy__.strategy(d) == "UnicodeDictStrategy"
        assert list(d) == ["a", "b"]

    def test_bug(self):
        import _pypyjson
        a =  """
        {
          "top": {
            "k": "8",
            "k": "8",
            "boom": 1
          }
        }
        """
        d = _pypyjson.loads(a)
        str(d)
        repr(d)

    def test_objdict_bug(self):
        import _pypyjson
        a = """{"foo": "bar"}"""
        d = _pypyjson.loads(a)
        d['foo'] = 'x'

        class Obj(object):
            pass

        x = Obj()
        x.__dict__ = d

        x.foo = 'baz'  # used to segfault on pypy3

        d = _pypyjson.loads(a)
        x = Obj()
        x.__dict__ = d
        x.foo # used to segfault on pypy3


    def test_copy(self):
        import _pypyjson
        d = _pypyjson.loads('{"a": 1}')
        d1 = d.copy()
        assert d1 == {"a": 1}

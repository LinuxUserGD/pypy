# -*- encoding: utf-8 -*-

class AppTestBuilders(object):
    spaceconfig = dict(usemodules=['__pypy__'])

    def test_simple(self):
        from __pypy__.builders import UnicodeBuilder
        b = UnicodeBuilder()
        b.append("abcä")
        b.append("123")
        b.append("1")
        s = b.build()
        assert s == "abcä1231"
        assert type(s) is str
        assert b.build() == s
        b.append("123")
        assert b.build() == s + "123"
        assert type(b.build()) is str

    def test_preallocate(self):
        from __pypy__.builders import UnicodeBuilder
        b = UnicodeBuilder(10)
        b.append("abc")
        b.append("123")
        s = b.build()
        assert s == "abc123"
        assert type(s) is str

    def test_append_slice(self):
        from __pypy__.builders import UnicodeBuilder
        b = UnicodeBuilder()
        b.append_slice("abcdefgh", 2, 5)
        raises(ValueError, b.append_slice, "1", 2, 1)
        s = b.build()
        assert s == "cde"
        assert type(s) is str
        b.append_slice("abc", 1, 2)
        s = b.build()
        assert s == "cdeb"
        assert type(s) is str
        b.append_slice(b'\xc3\xa4\xc4\x83\xc3\xa7'.decode("utf-8"), 1, 2)
        s = b.build()
        assert s.encode("utf-8") == b"cdeb\xc4\x83"

    def test_stringbuilder(self):
        from __pypy__.builders import StringBuilder
        b = StringBuilder()
        b.append("abc")
        b.append("123")
        assert len(b) == 6
        b.append("you and me")
        s = b.build()
        assert len(b) == 16
        assert s == "abc123you and me"
        assert b.build() == s

    def test_encode(self):
        from __pypy__.builders import UnicodeBuilder
        b = UnicodeBuilder()
        raises(UnicodeDecodeError, b.append, b'\xc0')

# trivial functions for testing

class AppTestFilter:
    def test_filter_no_arguments(self):
        raises(TypeError, filter)

    def test_filter_no_function_no_seq(self):
        raises(TypeError, filter, None)

    def test_filter_function_no_seq(self):
        raises(TypeError, filter, lambda x: x>3)

    def test_filter_function_too_many_args(self):
        raises(TypeError, filter, lambda x: x>3, [1], [2])

    def test_filter_no_function_list(self):
        assert [_f for _f in [1, 2, 3] if _f] == [1, 2, 3]

    def test_filter_no_function_tuple(self):
        assert [_f for _f in (1, 2, 3) if _f] == (1, 2, 3)

    def test_filter_no_function_string(self):
        assert [_f for _f in 'mystring' if _f] == 'mystring'

    def test_filter_no_function_with_bools(self):
        assert [_f for _f in (True, False, True) if _f] == (True, True)

    def test_filter_list(self):
        assert [x for x in [1, 2, 3, 4, 5] if x>3] == [4, 5]

    def test_filter_tuple(self):
        assert [x for x in (1, 2, 3, 4, 5) if x>3] == (4, 5)

    def test_filter_string(self):
        assert [x for x in 'xyzabcd' if x>'a'] == 'xyzbcd'

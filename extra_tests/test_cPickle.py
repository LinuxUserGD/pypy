import pytest
import pickle

def test_stack_underflow():
    with pytest.raises(pickle.UnpicklingError):
        pickle.loads("a string")

def test_bad_key():
    with pytest.raises(pickle.UnpicklingError) as excinfo:
        pickle.loads("v")
    assert str(excinfo.value) == "invalid load key, 'v'."

def test_find_global():
    import time, io
    entry = time.strptime('Fri Mar 27 22:20:42 2017')
    f = io.StringIO()
    pickle.Pickler(f).dump(entry)

    f = io.StringIO(f.getvalue())
    e = pickle.Unpickler(f).load()
    assert e == entry

    f = io.StringIO(f.getvalue())
    up = pickle.Unpickler(f)
    up.find_global = None
    with pytest.raises(pickle.UnpicklingError) as e:
        up.load()
    assert str(e.value) == "Global and instance pickles are not supported."

    f = io.StringIO(f.getvalue())
    up = pickle.Unpickler(f)
    up.find_global = lambda module, name: lambda a, b: (name, a, b)
    e = up.load()
    assert e == ('struct_time', (2017, 3, 27, 22, 20, 42, 4, 86, -1), {})

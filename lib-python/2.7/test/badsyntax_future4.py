"""This is a test"""
import __future__


def f(x):
    def g(y):
        return x + y
    return g

result = f(2)(4)

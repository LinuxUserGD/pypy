#
# test_codecmaps_jp.py
#   Codec mapping tests for Japanese encodings
#

from test import test_support
from test import multibytecodec_support
import unittest

class TestCP932Map(multibytecodec_support.TestBase_Mapping,
                   unittest.TestCase):
    encoding = 'cp932'
    mapfileurl = 'http://www.pythontest.net/unicode/CP932.TXT'
    supmaps = [
        ('\x80', '\u0080'),
        ('\xa0', '\uf8f0'),
        ('\xfd', '\uf8f1'),
        ('\xfe', '\uf8f2'),
        ('\xff', '\uf8f3'),
    ]
    for i in range(0xa1, 0xe0):
        supmaps.append((chr(i), chr(i+0xfec0)))


class TestEUCJPCOMPATMap(multibytecodec_support.TestBase_Mapping,
                         unittest.TestCase):
    encoding = 'euc_jp'
    mapfilename = 'EUC-JP.TXT'
    mapfileurl = 'http://www.pythontest.net/unicode/EUC-JP.TXT'


class TestSJISCOMPATMap(multibytecodec_support.TestBase_Mapping,
                        unittest.TestCase):
    encoding = 'shift_jis'
    mapfilename = 'SHIFTJIS.TXT'
    mapfileurl = 'http://www.pythontest.net/unicode/SHIFTJIS.TXT'
    pass_enctest = [
        ('\x81_', '\\'),
    ]
    pass_dectest = [
        ('\\', '\xa5'),
        ('~', '\u203e'),
        ('\x81_', '\\'),
    ]

class TestEUCJISX0213Map(multibytecodec_support.TestBase_Mapping,
                         unittest.TestCase):
    encoding = 'euc_jisx0213'
    mapfilename = 'EUC-JISX0213.TXT'
    mapfileurl = 'http://www.pythontest.net/unicode/EUC-JISX0213.TXT'


class TestSJISX0213Map(multibytecodec_support.TestBase_Mapping,
                       unittest.TestCase):
    encoding = 'shift_jisx0213'
    mapfilename = 'SHIFT_JISX0213.TXT'
    mapfileurl = 'http://www.pythontest.net/unicode/SHIFT_JISX0213.TXT'


def test_main():
    test_support.run_unittest(__name__)

if __name__ == "__main__":
    test_main()

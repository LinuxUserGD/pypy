

import unittest
from test import test_support

class TestFuture(unittest.TestCase):
    def assertType(self, obj, typ):
        self.assertTrue(type(obj) is typ,
            "type(%r) is %r, not %r" % (obj, type(obj), typ))

    def test_unicode_strings(self):
        self.assertType("", str)
        self.assertType('', str)
        self.assertType(r"", str)
        self.assertType(r'', str)
        self.assertType(""" """, str)
        self.assertType(''' ''', str)
        self.assertType(r""" """, str)
        self.assertType(r''' ''', str)
        self.assertType("", str)
        self.assertType('', str)
        self.assertType(r"", str)
        self.assertType(r'', str)
        self.assertType(""" """, str)
        self.assertType(''' ''', str)
        self.assertType(r""" """, str)
        self.assertType(r''' ''', str)

        self.assertType(b"", str)
        self.assertType(b'', str)
        self.assertType(br"", str)
        self.assertType(br'', str)
        self.assertType(b""" """, str)
        self.assertType(b''' ''', str)
        self.assertType(br""" """, str)
        self.assertType(br''' ''', str)

        self.assertType('' '', str)
        self.assertType('' '', str)
        self.assertType('' '', str)
        self.assertType('' '', str)

def test_main():
    test_support.run_unittest(TestFuture)

if __name__ == "__main__":
    test_main()

""" Python character mapping codec test

This uses the test codec in testcodec.py and thus also tests the
encodings package lookup scheme.

Written by Marc-Andre Lemburg (mal@lemburg.com).

(c) Copyright 2000 Guido van Rossum.

"""#"

import test.test_support, unittest

import codecs

# Register a search function which knows about our codec
def codec_search_function(encoding):
    if encoding == 'testcodec':
        from test import testcodec
        return tuple(testcodec.getregentry())
    return None

codecs.register(codec_search_function)

# test codec's name (see test/testcodec.py)
codecname = 'testcodec'

class CharmapCodecTest(unittest.TestCase):
    def test_constructorx(self):
        self.assertEqual(str('abc', codecname), 'abc')
        self.assertEqual(str('xdef', codecname), 'abcdef')
        self.assertEqual(str('defx', codecname), 'defabc')
        self.assertEqual(str('dxf', codecname), 'dabcf')
        self.assertEqual(str('dxfx', codecname), 'dabcfabc')

    def test_encodex(self):
        self.assertEqual('abc'.encode(codecname), 'abc')
        self.assertEqual('xdef'.encode(codecname), 'abcdef')
        self.assertEqual('defx'.encode(codecname), 'defabc')
        self.assertEqual('dxf'.encode(codecname), 'dabcf')
        self.assertEqual('dxfx'.encode(codecname), 'dabcfabc')

    def test_constructory(self):
        self.assertEqual(str('ydef', codecname), 'def')
        self.assertEqual(str('defy', codecname), 'def')
        self.assertEqual(str('dyf', codecname), 'df')
        self.assertEqual(str('dyfy', codecname), 'df')

    def test_maptoundefined(self):
        self.assertRaises(UnicodeError, str, 'abc\001', codecname)

def test_main():
    test.test_support.run_unittest(CharmapCodecTest)

if __name__ == "__main__":
    test_main()
